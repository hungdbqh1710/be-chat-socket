const AuthService = require('../../Services/AuthService');
const MailService = require('../../Services/MailService');
const { validationResult } = require('express-validator');
class AuthController {
  constructor() {
    this.authService = AuthService;
  }

  async register(req, res) {
    try {
      const errors = validationResult(req);
      const { email, name, url } = req.body;
      if (!errors.isEmpty()) {
        return res.status(422).json({'message': 'Error', data: errors.array()});
      }
      const result = await this.authService.register(req);
      if(result.status == 200){
          const msg = {
            reciver: email,
            subject: 'Confirm registration!',
        }
        const template = {
            data: {
                name,
                url,
                mail_token: result.data,
            },
            type: 'register',
        }
        await MailService.sendMail(msg, template);
      }
      return res.status(result.status).json({
        message: result.message
      });
    } catch (error) {
      console.log(error)
    }
  }

  async confirmRegister(req, res) {
    try{
      let result = await this.authService.confirmRegister(req);
      res.status(result.status).json(result);
    }
    catch(err){
      res.status(500);
    }
  }

  /**
   * Login 
   * @param {*} req 
   * @param {*} res 
   */
  async login(req, res) {
    try {
      process.send({ cmd: 'notifyRequest' });
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({'message': 'Error', data: errors.array()});
      }
      const result = await this.authService.login(req);
      return res.status(result.status).json(result);
    } catch (error) {
        console.log(error); 
    }
  }
  /**
   * Logout
   * @param {*} req 
   * @param {*} res 
   */
  async logout(req, res) {
    try{
      let result = await this.authService.logout(req);
      if(result.status == 'inactive')
      return res.status(200).send({
        message: 'Logout success',
        data: null
      });
      return res.status(500).send({
        message: 'login failed',
        data: null
      })
    }
    catch( err ){
      return res.status(500).send({
        message: 'login failed',
        data: err
      })
    }
  }
}

module.exports = new AuthController();
