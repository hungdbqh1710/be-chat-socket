const ChatService = require('../../Services/ChatService');
const { validationResult } = require('express-validator');
class ChatController {
    constructor() {
        this.chatService = ChatService;
    }

    /**
     * 
     * @param {*} req 
     * @param {*} res 
     */
    async getMsgByRoomId(req, res){
        try {
            const {id} = req.params;
            let msgs = await this.chatService.getMsgs(id,20);
            res.status(200).json({
                message: 'Get all messages success!',
                data: msgs
            })
        } catch (error) {
            res.status(400).json({
                message: 'Get all messages failed!',
                data: null
            })
        }
    }
    
    async createMsg(req, res) {
       try {
           let msg = await this.chatService.createMsg(req);
           res.status(200).json({
               data: msg,
               message: 'Create new msg success'
           })
       } catch (error) {
           res.status(500);
       }
   }

    async updateMsg(req, res) {
        try {
            let msg = await this.chatService.updateMsg(req);
            res.status(200).json({
                data: msg,
                message: 'Edit msg success'
            })
        } catch (error) {
            res.status(500);
        }
    }

    async deleteMsg(req, res) {
        try {
            let msg = await this.chatService.deleteMsg(req);
            res.status(msg.status).json(msg)
        } catch (error) {
            res.status(500);
        }
    }
}

module.exports = new ChatController();