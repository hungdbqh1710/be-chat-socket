const RoomService = require('../../Services/RoomService');
const { validationResult } = require('express-validator');
class RoomController {
    constructor() {
        this.roomService = RoomService;
    }

    async findRoom(req, res) {
        let room = await this.roomService.findRoom(req);
        res.status(room.status).json(room);
    }

    async getListByMe(req, res) {
        try {
            let rooms = await this.roomService.getListByMe(req);
            res.status(200).json(rooms);
        } catch (error) {
            res.status(500).json({
                message: 'Get list rooms failed',
                data: null
            })
        }
    }

    async getListAll(req, res) {
        try {
            let rooms = await this.roomService.getListAll(req);
            res.status(200).json(rooms);
        } catch (error) {
            res.status(500).json({
                message: 'Get list rooms failed',
                data: null
            })
        }
    }

    async createRoom(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
              return res.status(422).json({'message': 'Error', data: errors.array()});
            }
            const result = await this.roomService.create(req);
      
            return res.status(result.status).json(result);
        } catch (error) {
        console.log(error)
        }
    }

    async editRoom(req, res) {
        try {
            let result = await this.roomService.edit(req);
            return res.status(result.status).json(result);
        } catch (error) {
            res.status(500);
        }
    }

    async updateRoom(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
              return res.status(422).json({'message': 'Error', data: errors.array()});
            }
            const result = await this.roomService.update(req);
      
            return res.status(result.status).json(result);
        } catch (error) {
        console.log(error)
        }
    }

    async deleteRoom(req, res) {
       try {
           let result = await this.roomService.delete(req);
           res.status(result.status).json(result);
       } catch (error) {
           res.status(500);
       } 
    }

    async joinRoom(req, res) {
        try {
            let result = await this.roomService.joinRoom(req);
            res.status(result.status).json(result); 
        } catch (error) {
            res.status(500);
        }
    }
}

module.exports = new RoomController;