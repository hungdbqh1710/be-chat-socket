const UserService = require('../../Services/UserService');
const { validationResult } = require('express-validator');

class UserController {
    constructor() {
        this.userService = UserService;
    }

    async updateProfile(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({'message': 'Error', data: errors.array()});
            }
            const result = await this.userService.update(req);
            //console.log(result);
            return res.status(result.status).json(result);
        } catch (error) {
            res.status(500);
        }
    }
}

module.exports = new UserController();