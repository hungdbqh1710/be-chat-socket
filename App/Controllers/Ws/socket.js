const express = require('express');
const socket = require('socket.io');
const chatService = require('../../Services/ChatService');
const ChatModel = require('../../Models/Mongodb/ChatModel');
require('dotenv').config();

class SocketIO {
    constructor() {
        this.express = express();
        this.chatService = chatService;
    }

    async run() {
        try {
            const {
                PORT_SOCKET
            } = process.env;
            const http = require('http').createServer(this.express);

            const io = require('socket.io')(http, {
                pingTimeout: 30000,
                pingInterval: 60000
            });

            let connected = [];
            let users = [];
            let user_typing = [];
            io.on('connect', (socket) => {
                connected.push(socket.id);
                io.emit('client_connect', connected);

                // Add new user
                socket.on('new-user', data => {
                    users.push(data);
                    socket.nickname = data;
                    io.emit('users', users);
                    io.emit('joined', {
                        id: socket.id,
                        nickname: data,
                    });
                })

                this.chatService.getMsgs(20)
                .then(result => {
                    io.emit('new-message', result);
                });

                socket.on('message', data => {
                    let infoUser = {
                        socket_id: socket.id,
                        nickname: socket.nickname,
                        message: data
                    };
                    this.chatService.createMsg(infoUser)
                        .then(result => {
                            infoUser._id = result._id;
                            infoUser.date = result.created;
                            infoUser.isUpdate = false;
                            io.emit('chat', infoUser);
                        })
                        .catch(err => {
                            console.log(err);
                        })
                });

                socket.on('edit-message', data => {
                    let infoMsg = {
                        socket_id: socket.id,
                        nickname: socket.nickname,
                        message: data.message
                    };
                    this.chatService.updateMsg(data)
                    .then(result => {
                        infoMsg._id = result._id;
                        infoMsg.date = result.created;
                        infoMsg.isUpdate = true;
                        io.emit('chat', infoMsg);
                    })
                    .catch(err => {
                        console.log(err);
                    })
                })

                socket.on('typing', function (data) {
                    if (!user_typing.includes(data)) {
                        user_typing.push(data);
                        let dt = {
                            id: socket.id,
                            user_typing
                        }
                        io.emit('typing', dt);
                    }
                });

                socket.on('untyping', function (data) {
                    let index = user_typing.indexOf(data);
                    user_typing.splice(index, 1);
                    io.emit('untyping', {
                        id: socket.id,
                        user_typing
                    });
                });

                socket.on('processUserInfo', data => {
                    io.sockets.emit('updateUserInfo', data);
                });

                socket.on('proccessRoom', data => {
                    io.sockets.emit('updateRoom', data);
                });

                socket.on('proccessEditMsg', data => {
                    io.sockets.emit('updateEditMsg', data);
                });

                socket.on('proccessDeleteMsg', data => {
                    io.sockets.emit('updateDeleteMsg', data);
                });

                socket.on('proccessNewMsg', data => {
                    io.sockets.emit('updateNewMsg', data);
                });
            });

            http.listen(PORT_SOCKET, () => {
                console.log(`Socket run at port: ${PORT_SOCKET}`);
            });
        } catch (err) {
            console.log(err);
        }
    }
}
module.exports = new SocketIO;