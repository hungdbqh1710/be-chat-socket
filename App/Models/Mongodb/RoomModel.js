const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RoomModel = new Schema({
    name: String,
    admin: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Users'
    },
    users: [{
        user_id: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'Users'
        },
        name: String,
        status: {
            type: String,
            default: 'active'
        },
        created: {
            type: Date,
            default: Date.now
        }
    }],
    image: String,
    status: {
        type: String,
        default: 'active'
    },
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date,
        default: Date.now()
    },
})

module.exports = mongoose.model('Rooms', RoomModel);

