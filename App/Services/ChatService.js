const ChatModel = require('../Models/Mongodb/ChatModel');

class ChatService {
    constructor() {
        this.chatModel = ChatModel;
    }
    
    async createMsg(req) {
        try{
            const { user } = req;
            const data = req.body;
            let new_msg = await this.chatModel.create({
                sender_id: {
                    id: user._id,
                    name: user.name,
                    avatar: user.avatar
                },
                message: data.message,
                room_id: data.room_id
            });
            return new_msg;
        }
        catch(error){
            console.log(error);
        }
    }

    async updateMsg(req) {
        try{
            const data = req.body;
            let update_msg = await this.chatModel.update({_id: data.id},{
                message: data.message,
                updated: Date.now()
            }).exec();
            let new_msg = await this.chatModel.findOne({_id: data.id}).exec();
            return new_msg;
        }
        catch(error){
            console.log(error);
        }
    }

    async deleteMsg(req) {
        try{
            const { msg_id } = req.params;
            let del_msg = await this.chatModel.deleteOne({_id: msg_id}).exec();
            return {
                message: del_msg.deletedCount == 1 ? 'Delete message success !' : 'Delete message failed !' ,
                status: del_msg.deletedCount == 1 ? 200 : 500,
                data: null
            }
        }
        catch(error){
            console.log(error);
        }
    }

    async getMsgs(room_id, limit) {
        try {
            return await this.chatModel.find({room_id}).sort('-created').limit(limit).exec();
        } catch (error) {
            return null
        }
    }
}

module.exports = new ChatService();

