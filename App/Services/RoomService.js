const RoomModel = require('../Models/Mongodb/RoomModel');
const CommonService = require('../Services/CommonService');

class RoomService {
    constructor() {
        this.roomModel = RoomModel;
    }

    async getListByMe(req) {
        try {
            const {userId} = req;
            let rooms = await this.roomModel.find({status: 'active', "users.user_id": { $all: [ userId ] }}).exec();
            return {
                message: 'Get list room success',
                data: rooms
            }
        } catch (error) {
            console.log(error)
        }
    }

    async getListAll(req) {
        try {
            const {userId} = req;
            let rooms = await this.roomModel.find({status: 'active'}).populate('').exec();
            return {
                message: 'Get all list room success',
                data: rooms
            }
        } catch (error) {
            console.log(error)
        }
    }

    async create(req){
        try {
            const {name, image} = req.body;
            const { userId, user } = req;
            let img = image ? await CommonService.uploadImage(image) : '';
            let room = await this.roomModel.create(
                {
                    name, 
                    image: img, 
                    admin: userId
                }
            );
            await this.roomModel.update({_id: room._id},{
                $push: { 
                    users: {
                        user_id: userId,
                        name: user.name,
                        status: 'active'
                    }
                }}).exec();
            room = await this.roomModel.findOne({_id: room._id}).exec();
            return {
                status: 200,
                message: 'Create room success',
                data: room
            }
        } catch (error) {
            return {
                status: 500,
                message: 'Create room failed',
                data: error
            }
        }
    }

    async edit(req){
        try {
            const {id} = req.params;
            const {userId} = req;
            let room = await this.roomModel.findOne({_id: id, "users.user_id": userId}).exec();
            return {
                status: 200,
                message: 'Get data edit success!',
                data: room
            }
        } catch (error) {
            return {
                status: 400,
                message: 'Room is not exits',
                data: null
            }
        }
    }

    async update(req){
        try {
            const { token, userId } = req;
            const {id} = req.params;
            const {name, image, old_img} = req.body;
            let img = image ? await CommonService.uploadImage(image) : old_img;
            await this.roomModel.update({_id: id, "users.user_id": userId }, {name, image: img}).exec();
            let room = await this.roomModel.findOne({_id: id}).exec();
            return {
                status: 200,
                message: 'Update data success!',
                data: room
            }
        } catch (error) {
            return {
                status: 500,
                message: 'Update data failed',
                data: error
            }
        }
    }

    async delete(req){
        try {
            const {id} = req.params;
            const { token, userId } = req;
            let room = await this.roomModel.deleteOne({_id: id, "users.user_id": userId }).exec();
            return {
                status: room.deletedCount ? 200 : 400,
                message: room.deletedCount ? 'Delete data success!' : 'Room is not exist',
            }
        } catch (error) {
            return {
                status: 500,
                message: 'Delete data failed',
                data: error
            }
        }
    }

    async findRoom(req) {
        try {
            const {id} = req.params;
            let room = await this.roomModel.findOne({_id: id}).exec();
            if(!room){
                return {
                    status: 400,
                    message: 'Room is not exist',
                    data: null
                }
            }
            return {
                status: 200,
                message: 'Get room detail success',
                data: room
            }
        } catch (error) {
            
        }
    }

    async joinRoom(req) {
        try {
            const {room_id, user} = req.body;
            let result = await this.roomModel.updateOne({_id: room_id, "users.user_id": {$nin: [ user.id ] }}, {
                $push: {
                    users: {
                        user_id: user.id,
                        name: user.name,
                        status: 'active'
                    }
                }
            }).exec();
            return {
                status: 200,
                data: null,
                message: 'Joined success ! Chat now'
            }
        } catch (error) {
            return {
                message: 'Join fail',
                status: 400,
                data: null
            }
        }
    }
}

module.exports = new RoomService;