const UserModel = require('../Models/Mongodb/UserModel');
const dotenv = require('dotenv');
const TokenModel = require('../Models/Mongodb/TokenModel');
const jwt = require('json-web-token');
const bcrypt = require('bcryptjs');
const CommonService = require('../Services/CommonService');
const {ENCODE_KEY} = process.env;

class UserService {
    constructor() {
        this.userModel = UserModel;
        this.tokenModel = TokenModel;
    }
    async getUserbyId(id){
        try {
            const user = await this.userModel.findOne({_id: id}).exec();
            return user;
        } catch (error) {
            
        }  
    }

    async update(req){
        try {
            const {id} = req.params;
            const { username, new_psw, name, current_psw, confirm_psw, avatar }  = req.body;
            const { USER_PASSWORD_SALT_ROUNDS: saltRounds = 10 } = process.env;
            let user = await this.userModel.findOne({_id: id}).exec();
            if(user){
                let image = avatar ? await CommonService.uploadImage(avatar) : user.avatar;

                await user.update({
                    username,name,avatar: image
                }).exec();
                user.name = name;
                user.username = username;
                user.avatar = image;
                if( new_psw && current_psw && confirm_psw){
                    if(await bcrypt.compare(current_psw, user.password)){
                        const passwordHash = await bcrypt.hash(new_psw, +saltRounds);
                        user.password = passwordHash;
                    }
                }

                user.save(function(err, user){
                    if(err) return {
                        status: 500,
                        message: "Update profile failed",
                        data: null
                    }
                })
                let token = await jwt.encode(ENCODE_KEY, {
                    id: user._id,
                    name: user.name,
                    username: user.username,
                    avatar: user.avatar,
                    create: user.created,
                    email: user.email,
                    timestamp: new Date().getTime()
                });
                let token_exist = await this.tokenModel.findOne({user_id: user._id}).exec();
                if(token_exist){
                    token_exist.token = token.value;
                    token_exist.updated = Date.now();
                    token_exist.save();  
                }

                return {status: 200, 'message': 'user-update-success', data: token.value}
                // cache
                //await CacheService.removeCache('users');
                
            }
            return {
                status: 400,
                data: null
            }
        } catch (error) {
            
        }  
    }
}
module.exports = new UserService();