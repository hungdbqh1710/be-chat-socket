const { body } = require('express-validator');

module.exports = [
  body('name')
    .exists()
    .withMessage('Name is required')
    .not()
    .isEmpty()
    .withMessage('Name is required'),
  body('username')
    .exists()
    .withMessage('Username is required')
    .not()
    .isEmpty()
    .withMessage('Username is required'),
  body('email')
    .exists()
    .withMessage('Email is required')
    .isEmail()
    .withMessage('Email is not valid')
    .not()
    .isEmpty()
    .withMessage('Emal is required'),
  body('password')
    .exists()
    .withMessage('Password is required')
    .isLength({
      min: 8
    })
    .withMessage('Password min length is 8')
];
