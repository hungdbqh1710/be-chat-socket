const express = require('express');
var cors = require('cors');
const SocketIO = require('../App/Controllers/Ws/socket');
const Mongodb = require('../App/Models/Mongodb/index');
require('dotenv').config();

global.Env = process.env;

const app = express();

app.use(cors({
  origin: '*',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE'
}));

app.use(express.json({limit: '10mb', extended: true}));
app.use(express.urlencoded({limit: '10mb', extended: true}))

app.use((err, req, res, next) => {
  if (! err) {
      return next();
  }

  res.status(500);
  res.send(err);
}); 

app.listen(Env.PORT, () => {
  console.log(`Server run at port: ${Env.PORT}`);
});
Mongodb.connect();
SocketIO.run();

module.exports = app;
