const express = require('express');
const router = express.Router();
const AuthController = require('../App/Controllers/Http/AuthController');
const AuthMiddleware= require('../App/Middlewares/AuthMiddleware');
const registerValidator = require('../App/Validators/registerValidator');
const loginValidator = require('../App/Validators/loginValidator');

// Router register.
router.post('/register', registerValidator, (req, res) => AuthController.register(req, res));

router.post('/confirm-register', (req, res) => AuthController.confirmRegister(req, res));

// Router login.
router.post('/login', loginValidator, (req, res) => AuthController.login(req, res));

// Routes need auth
router.use((req, res, next) => AuthMiddleware.auth(req, res, next));

router.post('/logout', (req, res) => AuthController.logout(req, res));

module.exports = router;
