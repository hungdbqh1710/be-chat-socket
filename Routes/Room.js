const express = require('express');
const router = express.Router();
const RoomController = require('../App/Controllers/Http/RoomController');
const ChatController = require('../App/Controllers/Http/ChatController');
const AuthMiddleware= require('../App/Middlewares/AuthMiddleware');
const cRoomValidator = require('../App/Validators/createRoomValidator');

// Routes need auth

router.use((req, res, next) => AuthMiddleware.auth(req, res, next));
router.get('/me', (req, res) => RoomController.getListByMe(req, res));
router.get('/all', (req, res) => RoomController.getListAll(req, res));
router.get('/:id/detail', (req, res) => RoomController.findRoom(req, res));
router.post('/create', cRoomValidator, (req, res) => RoomController.createRoom(req, res));
router.post('/:id/join', (req, res) => RoomController.joinRoom(req, res));
router.get('/:id/edit', (req, res) => RoomController.editRoom(req, res));
router.patch('/:id/update', cRoomValidator, (req, res) => RoomController.updateRoom(req, res));
router.delete('/:id/delete', cRoomValidator, (req, res) => RoomController.deleteRoom(req, res));
router.get('/:id/msgs', (req, res) => ChatController.getMsgByRoomId(req, res));
router.post('/:id/msg/new', (req, res) => ChatController.createMsg(req, res));
router.patch('/:id/msg/:msg_id/update', (req, res) => ChatController.updateMsg(req, res));
router.delete('/:id/msg/:msg_id/delete', (req, res) => ChatController.deleteMsg(req, res));
module.exports = router;
