const express = require('express');
const profileValidator = require('../App/Validators/profileValidator');
const AuthMiddleware= require('../App/Middlewares/AuthMiddleware');
const UserController= require('../App/Controllers/Http/UserController');
const router = express.Router();

router.use((req, res, next) => AuthMiddleware.auth(req, res, next));

// Update
router.patch('/:id/update', profileValidator, (req, res) => UserController.updateProfile(req, res));


module.exports = router;
