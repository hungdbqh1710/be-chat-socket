const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;

/* if(cluster.isMaster){
    masterPoccess();
}else{
    workerProcess();
}

function masterPoccess(){
    console.log(`Master ${process.pid} is running`);
    
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }
}

function workerProcess() {
    console.log('Children process !' + process.pid);
    // Run express app.
    require('./Config/app');

    // Import routes.
    require('./Config/router');

    // Import Errors.
    require('./Config/error');
} */

// Run express app.
require('./Config/app');

// Import routes.
require('./Config/router');

// Import Errors.
require('./Config/error');