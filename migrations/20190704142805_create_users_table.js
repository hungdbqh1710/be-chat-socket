
exports.up = function(knex) {
  return knex.schema.createTable('users', (table) => {
    table.increments();
    table.string('name', 100);
    table.string('username').unique().index();
    table.string('email').unique().index();
    table.string('mailtoken').unique();
    table.string('password');
    table.string('avatar');
    table.timestamps(true, true);
  })
};

exports.down = function(knex) {
  return knex.schema
      .dropTable("users");
};
