const socket = io('http://localhost:5002');
const id = socket.id;
let nickname = '';
/* while (!nickname) {
    nickname = prompt('Enter your nickname: ');
} */
socket.nickname = 'nickname';
socket.emit('new-user', nickname);
socket.on('users', data => {
    console.log(data);
});

socket.on('joined', data => {
    let check = data.id == socket.id;
    /* $('#content').prepend(`<div class="text-center my-4"><small class="text-muted">
        <i>${check ? 'You have ' : data.nickname + ' has '} joined group</i>
        </small><div>`); */
})

socket.on('new-message', msgs => {
    for (let i = msgs.length-1; i >=0; i--) {
        this.showMsgs(msgs[i]);   
    }
    //$( "#content" ).scrollTop( $( "#content" )[0].scrollHeight );
    $( "div.messages" ).scrollTop( $( "div.messages" )[0].scrollHeight );
})
// typing

$('#chat-input').keypress((event) => {
    if (event.which === 13) {
        event.preventDefault();
        typing = false;
        const message = $('#chat-input').val();
        $( "div.messages" ).scrollTop( $( "div.messages" )[0].scrollHeight );
        if (message) {
            socket.emit('message', message);
            socket.emit('untyping', '');
            $('#chat-input').val('');
            $( "div.messages" ).scrollTop( $( "div.messages" )[0].scrollHeight );
            //document.getElementById("content").scrollTo(0, 500);
        }
    }
})

socket.on('chat', (data) => {
    if(data.isUpdate){
        console.log(data);
        $('.msg-'+data._id).html(data.message);
    }else{
        showMsgs(data);
    }
    $( "div#content" ).scrollTop( $( "div#content" )[0].scrollHeight );
})

var timeout;
var typing = false;
let user_typing = [];

$("input").keypress(function() {
    if($(this).val().length > 0 && !typing ){
        typing = true;
        socket.emit('typing', {id: socket.id, nickname: socket.nickname});
    }
})

$("input").blur(function(){
    if(!$(this).val().length){
        typing = false;
        socket.emit('untyping', {id: socket.id, nickname: socket.nickname});
    }
});

socket.on('typing',function(data) {
    if(data.id != socket.id){
        let names='';
        for( let i=0; i< data.user_typing.length; i++){
            if(data.user_typing[i].id != socket.id){
                names+=data.user_typing[i].nickname+', ';
            }
        }
        $(".chatApp__convTyping").prepend(`
        <div>${names} writing
            <span class="chatApp__convTypingDot"></span>
        </div>
        `)
    }
});

socket.on('untyping', function(data) {
    if(data.id != socket.id){
        $(".chatApp__convTyping").html('');
        let names='';
        if(data.user_typing.length){
            for( let i=0; i< data.user_typing.length; i++){
                if(data.user_typing[i].id != socket.id){
                    names+=data.user_typing[i].nickname+', ';
                }
            }
            if(names.length){
                return $(".chatApp__convTyping").prepend(`<div>${names} writing <span class='chatApp__convTypingDot'></span></div>`)
            }
        }
        return $(".chatApp__convTyping").prepend('');
    }
});

$('[data-toggle="tooltip"]').tooltip();

function showMsgs(msg) {
    let html = ``;
    let check = msg.socket_id == socket.id;
    let messagePosition = (check ? 'chatApp__convMessageItem--right' : 'chatApp__convMessageItem--left');
    html +=`<div class="chatApp__convMessageItem ${messagePosition} clearfix" ${}>
                <img src='./images/user.png' alt='' class="chatApp__convMessageAvatar" />
                <div class="chatApp__convMessageValue msg-${msg._id}">${msg.message}</div>
                <div class='d-none cls-${msg._id} mt-2 ${check ? 'mr-4 float-right' : ''}'>
                    <button class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" title="Edit message!" onclick="editMsg('${msg._id}', '${msg.message}')">
                        <i class="fa fa-edit mx-2" aria-hidden="true" ></i></button>
                    <button type="button" class="btn btn-outline-danger btn-sm" data-toggle="tooltip" title="Delete message!">
                        <i class="fa fa-trash mx-2" aria-hidden="true" ></i>
                    </button>
                </div>
            </div>`
    $('#content').prepend(html);
}

function actions(id){
    if($(".cls-"+id).hasClass( "d-none" )){
        $(".cls-"+id).removeClass("d-none");
    }else{
        $(".cls-"+id).addClass("d-none");
    }
}

function editMsg(id, msg) {
    let msg_edit = prompt('Edit your message: ', msg);
    if(msg_edit != null){
        let data_msg = {
            id,
            nickname: socket.nickname,
            message: msg_edit,
            updated: Date.now
        }
        socket.emit('edit-message', data_msg);
    }
}

